"use client"; // Ceci est un composant client 👈🏽
import React, { useState } from "react";

const NavBar = () => {
  return (
    <nav className="navbar navbar-light bg-light">
      <span className="font-semibold text-lg">La liste des tâches à faire</span>
    </nav>
  );
};

const Liste = ({ todos, check, renameTache }) => {
  const Tache = ({ todo, check, renameTache }) => {
    const [editMode, setEditMode] = useState(false);
    const [editedText, setEditedText] = useState(todo.text);

    const handleEditClick = () => {
      setEditMode(true);
    };

    return (
      <div className="flex items-center gap-4 justify-between w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring">
        {editMode ? (
          <input
            type="text"
            value={editedText}
            onChange={(e) => setEditedText(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                renameTache({ ...todo, text: editedText });
                setEditMode(false);
              }
            }}
            onBlur={() => {
              setEditMode(false);
              setEditedText(todo.text);
            }}
            autoFocus
          />
        ) : (
          <span>{todo.text}</span>
        )}

        <div className="flex gap-x-2">
          <button
            onClick={() => handleEditClick()}
            className="text-gray-500 transition-colors duration-200 dark:hover:text-yellow-500 dark:text-gray-300 hover:text-yellow-500 focus:outline-none"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
              />
            </svg>
          </button>
          <button
            onClick={() => check(todo)}
            className="text-gray-500 transition-colors duration-200 dark:hover:text-green-500 dark:text-gray-300 hover:text-green-500 focus:outline-none"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              role="img"
              className="w-5 h-5"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                fill="currentColor"
                d="M9 16.17L4.83 12l-1.42 1.41L9 19L21 7l-1.41-1.41z"
              ></path>
            </svg>
          </button>
        </div>
      </div>
    );
  };
  return todos.map((todo) => {
    return (
      <Tache
        key={todo.id}
        todo={todo}
        check={check}
        renameTache={renameTache}
      />
    );
  });
};

const TachesTerminees = ({ liste, check, supprimer }) => {
  return (
    <ul id="done-items" className="list-unstyled">
      {liste.map((todo) => {
        return (
          <li
            key={todo.id}
            className="flex justify-between items-center gap-3 w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
          >
            <label onClick={() => check(todo)}>{todo.text}</label>
            <button
              className={`text-gray-500 transition-colors duration-200 dark:hover:text-red-500 dark:text-gray-300 hover:text-red-500 focus:outline-none           }`}
              onClick={() => supprimer(todo.id)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-5 h-5"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                />
              </svg>
            </button>
          </li>
        );
      })}
    </ul>
  );
};

const TodoApp = () => {
  const [todos, setTodos] = useState([
    { id: 1, text: "Tâche 1", estTerminee: false },
    { id: 2, text: "Tâche 2", estTerminee: false },
    { id: 3, text: "Tâche 3", estTerminee: false },
    { id: 4, text: "Tâche terminée 1", estTerminee: true },
    { id: 5, text: "Tâche terminée 2", estTerminee: true },
  ]);

  const [input, setInput] = useState("");

  const ajouterTache = (e) => {
    if (e.key === "Enter") {
      const nouvelleTache = {
        id: todos.length + 1,
        text: input,
        estTerminee: false,
      };
      setTodos([...todos, nouvelleTache]);
      setInput("");
    }
  };

  const basculerTache = (tache) => {
    const tachesMisesAJour = todos.map((t) => {
      if (t.id === tache.id) {
        return { ...t, estTerminee: !t.estTerminee };
      }
      return t;
    });

    setTodos(tachesMisesAJour);
  };

  const renameTache = (tache) => {
    const tachesMisesAJour = todos.map((t) => {
      if (t.id === tache.id) {
        return { ...t, text: tache.text };
      }
      return t;
    });
    setTodos(tachesMisesAJour);
  };
  const tachesTerminees = todos.filter((todo) => todo.estTerminee);
  const tachesNoTerminees = todos.filter((todo) => !todo.estTerminee);

  const afficherTexteTermine = tachesTerminees.length > 0;

  const supprimerTache = (id) => {
    setTodos((prevTodos) => prevTodos.filter((todo) => todo.id !== id));
  };

  return (
    <div>
      <div className="flex py-4 border-b border-solid dark:border-slate-200 border-gray-500 rounded-t justify-center w-full">
        <div className="container flex gap-3 items-center px-5">
          <button className="dark:bg-gray-600 border dark:border-slate-200 border-gray-500 flex justify-center items-center h-7 w-7 rounded-md hover:opacity-90">
            <div className="text-[#07074D] dark:text-gray-100">
              <svg
                width="8"
                height="8"
                viewBox="0 0 10 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0.279337 0.279338C0.651787 -0.0931121 1.25565 -0.0931121 1.6281 0.279338L9.72066 8.3719C10.0931 8.74435 10.0931 9.34821 9.72066 9.72066C9.34821 10.0931 8.74435 10.0931 8.3719 9.72066L0.279337 1.6281C-0.0931125 1.25565 -0.0931125 0.651788 0.279337 0.279338Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0.279337 9.72066C-0.0931125 9.34821 -0.0931125 8.74435 0.279337 8.3719L8.3719 0.279338C8.74435 -0.0931127 9.34821 -0.0931123 9.72066 0.279338C10.0931 0.651787 10.0931 1.25565 9.72066 1.6281L1.6281 9.72066C1.25565 10.0931 0.651787 10.0931 0.279337 9.72066Z"
                  fill="currentColor"
                />
              </svg>
            </div>
          </button>
          <h3 className="text-2xl font-semibold text-gray-800 dark:text-white">
            Todo List
          </h3>
        </div>
      </div>
      <div className="flex flex-col items-center w-full h-[95vh]">
        <div className="container py-6 px-5">
          <div className="flex flex-row justify-between">
            <div className="">
              <h2 className="font-semibold text-lg">Ajouter</h2>
              <div className="todolist not-done">
                <input
                  type="text"
                  className="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                  placeholder="Ajouter une tâche"
                  value={input}
                  onChange={(e) => setInput(e.target.value)}
                  onKeyDown={(e) => ajouterTache(e)}
                />
                <div className="pt-4 pb-2">
                  <NavBar />
                </div>
                <Liste
                  todos={tachesNoTerminees}
                  check={basculerTache}
                  renameTache={renameTache}
                />
                <div className="py-2">
                  <strong>
                    <span className="count-todos">
                      {tachesNoTerminees.length}{" "}
                    </span>
                  </strong>
                  Tâches restantes
                </div>
              </div>
            </div>

            <div>
              {afficherTexteTermine && <h3 className="font-semibold text-lg">Terminé</h3>}
              <TachesTerminees
                liste={tachesTerminees}
                check={basculerTache}
                supprimer={supprimerTache}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TodoApp;
